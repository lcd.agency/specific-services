<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIntegratesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('integrates', function (Blueprint $table) {
            $table->id();
            $table->text('FACEBOOK_CLIENT_ID')->nullable();
            $table->text('FACEBOOK_CLIENT_SECRET')->nullable();
            $table->text('FACEBOOK_REDIRECT_URL')->nullable();
            $table->text('GOOGLE_CLIENT_ID')->nullable();
            $table->text('GOOGLE_CLIENT_SECRET')->nullable();
            $table->text('GOOGLE_REDIRECT')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('integrates');
    }
}