@extends('layouts.auth')
@section('title', 'Set Email Address')
@section('content')
<div class="card">
    <div class="card-body">
        <div class="text-center">
        <img src="{{url('/cdn/user/'.Auth::user()->avatar)}}" width="80" height="80" class="mb-3 rounded-circle">
        <h4 class="card-title mb-2">{{__('Add email address') }}</h4>
        <p class="text-muted">Please enter your email address.</p>
        </div>
        <form action="{{ route('addition.email') }}" method="POST" class="my-login-validation">
            @csrf
           <div class="form-group">
                <label for="email">{{ __('E-Mail Address :') }} <span class="fsgred">*</span></label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group m-0">
                <button type="submit" name="SubBtn" class="btn btn-primary btn-block"> {{ __('Submit') }}</button>
            </div>
        </form>
    </div>
</div>
@endsection