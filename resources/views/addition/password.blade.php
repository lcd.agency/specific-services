@extends('layouts.auth')
@section('title', 'Set account password')
@section('content')
<div class="card">
    <div class="card-body">
        <div class="text-center">
        <img src="{{url('/cdn/user/'.Auth::user()->avatar)}}" width="80" height="80" class="mb-3 rounded-circle">
        <h4 class="card-title mb-2">{{__('Set account password') }}</h4>
        <p class="text-muted">Please enter a password to login next time</p>
        </div>
        <form action="{{ route('addition.password') }}" method="POST" class="my-login-validation" novalidate="">
            @csrf
            <div class="form-group">
                <label for="password">{{ __('Password :') }} <span class="fsgred">*</span></label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password">{{ __('Confirm Password :') }} <span class="fsgred">*</span></label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="password_confirmation">
            </div>
            <div class="form-group m-0">
                <button type="submit" name="SubBtn" class="btn btn-primary btn-block"> {{ __('Submit') }}</button>
            </div>
        </form>
    </div>
</div>
@endsection