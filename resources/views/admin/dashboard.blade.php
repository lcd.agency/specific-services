@extends('admin.layouts.app')
@section('title', 'Admin Dashboard')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h3 class="page-title text-truncate text-dark font-weight-medium mb-1">Welcome back {{Auth::user()->firstname}} !</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3">
            <div class="card border-right bg-primary">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-white mb-1 font-weight-medium">{{$open_tickets->count()}}</h2>
                            </div>
                            <h6 class="stfont-weight-norma text-white mb-0 w-100 text-truncate">{{__('Opened Tickets')}}</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-white"><i class="fa fa-tag"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card border-right  bg-success">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-white mb-1 font-weight-medium">{{$answered_tickets->count()}}</h2>
                            </div>
                            <h6 class="stfont-weight-norma text-white mb-0 w-100 text-truncate">Answered Tickets</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-white"><i class="fa fa-tag"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card border-right  bg-danger">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-white mb-1 font-weight-medium">{{$closed_tickets->count()}}</h2>
                            </div>
                            <h6 class="font-weight-norma text-white mb-0 w-100 text-truncate">{{__('Closed Tickets')}}</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-white"><i class="fa fa-tag"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card border-right  bg-secondary">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-white mb-1 font-weight-medium">{{$total_tickets->count()}}</h2>
                            </div>
                            <h6 class="stfont-weight-norma text-white mb-0 w-100 text-truncate">{{__('Total Tickets')}}</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-white"><i class="fa fa-tag"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($open_tickets->count() > 0)
    <div class="alert alert-info">
        You have <strong> {{$open_tickets->count()}} opened </strong> tickets waiting for response
    </div>
    @endif
</div>
@stop