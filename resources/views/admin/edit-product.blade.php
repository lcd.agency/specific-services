@extends('admin.layouts.app')
@section('title', 'Edit Product')
@section('content')
<div class="container-fluid">
    <a href="{{url('/admin/products')}}" class="btn btn-outline-secondary btn-sm"><i class="fa fa-chevron-left "></i> Back to Products page</a>
    <hr/>
    <div class="row">
        <div class="col-6 m-auto">
            @if(session('success'))
            <div class="note note-success">
                <span class="icon"><i class="fa fa-check"></i></span>
                {{session('success')}}
            </div>
            @elseif(session('error'))
            <div class="note note-danger">
                <span class="icon"><i class="fa fa-exclamation-triangle"></i></span>
                {{session('error')}}
            </div>
            @endif
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{__('Edit Product ')}}</h4>
                    <form action="{{route('admin/products/edit')}}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{$product->id}}">
                        <div class="form-group">
                            <label for="product_name"><small>{{__('Product Name :')}}</small></label>
                            <input type="text" name="product_name" class="form-control @error('product_name') is-invalid @enderror" value="{{$product->product_name}}" required>
                            @error('product_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm">{{__('Save')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop