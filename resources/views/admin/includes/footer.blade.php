<script src="{{ asset('static/bootstrap/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('static/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ asset('static/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('static/admin/js/app-style-switcher.js') }}"></script>
<script src="{{ asset('static/admin/js/feather.min.js') }}"></script>
<script src="{{ asset('static/admin/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('static/admin/js/sidebarmenu.js') }}"></script>
<script src="{{ asset('static/admin/js/custom.min.js') }}"></script>
<script src="{{ asset('static/admin/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('static/admin/js/pages/datatable/datatable-basic.init.js') }}"></script>
<script src="{{ asset('static/admin/extra-libs/ckeditor/ckeditor.js')}}"></script>
<script>
CKEDITOR.replace("ckeditor1");
CKEDITOR.replace("ckeditor2");
</script>