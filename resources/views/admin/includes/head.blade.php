<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{$info->site_name}} - @yield('title')</title>
<link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
<link rel="stylesheet" href="{{ asset('static/bootstrap/css/bootstrap.min.css') }}">
<link href="{{ asset('static/admin/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
<link href="{{ asset('static/admin/css/style.css') }}" rel="stylesheet">