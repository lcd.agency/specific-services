<header class="topbar" data-navbarbg="skin6">
    <nav class="navbar top-navbar navbar-expand-md">
        <div class="navbar-header" data-logobg="skin6">
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)">
                <i class="ti-menu ti-close"></i>
            </a>
            <div class="navbar-brand">
                <a href="{{url('admin/dashboard')}}">
                    <b class="logo">
                        <img src="{{ asset('images/logo.png')}}" alt="{{$info->site_name}}" width="180"/>
                    </b>
                </a>
            </div>
            <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="ti-more"></i></a>
        </div>
        <div class="navbar-collapse collapse" id="navbarSupportedContent">
            <ul class="navbar-nav float-left mr-auto ml-3 pl-1"></ul>
            <ul class="navbar-nav float-right">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{asset('/cdn/user/'.Auth::user()->avatar)}}" alt="user" class="rounded-circle" width="40">
                        <span class="ml-2 d-none d-lg-inline-block">
                        <span class="text-dark">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</span>
                        <i data-feather="chevron-down" class="svg-icon"></i>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                    <a class="dropdown-item p-3" href="{{url('/dashboard')}}">
                        <i class="fa fa-user"></i>
                        {{__('User Panel')}}
                    </a>
                    <a class="dropdown-item p-3" href="{{url('/edit-profile')}}">
                        <i class="fa fa-edit"></i>
                        {{__('Edir profile')}}
                    </a>
                    <div class="dropdown-divider mt-0 mb-0"></div>
                  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();"><i data-feather="power"
                                        class="svg-icon mr-2 ml-1"></i> {{ __('Logout') }}</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
                </div>
            </li>
        </ul>
    </div>
</nav>
</header>