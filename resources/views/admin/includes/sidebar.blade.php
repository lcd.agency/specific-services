<aside class="left-sidebar" data-sidebarbg="skin6">
    <div class="scroll-sidebar" data-sidebarbg="skin6">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="{{url('/admin/dashboard')}}" aria-expanded="false">
                        <i data-feather="home" class="feather-icon"></i>
                        <span class="hide-menu">{{__('Dashboard')}}</span>
                    </a>
                </li>
                <li class="list-divider"></li>
                <li class="nav-small-cap">
                    <span class="hide-menu">{{__('Importants')}}</span>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link" href="{{url('/admin/tickets')}}" aria-expanded="false">
                        <i data-feather="tag" class="feather-icon"></i>
                        <span class="hide-menu">{{__('Manage Tickets')}}
                        </span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link" href="{{url('/admin/users')}}" aria-expanded="false">
                        <i class="fa fa-users"></i>
                        <span class="hide-menu">{{__('Manage Users')}}</span>
                    </a>
                </li>
                 <li class="list-divider"></li>
                <li class="nav-small-cap">
                    <span class="hide-menu">{{__('Webiste')}}</span>
                </li>
                 <li class="sidebar-item">
                    <a class="sidebar-link" href="{{url('/admin/products')}}" aria-expanded="false">
                        <i class="fa fa-server"></i>
                        <span class="hide-menu">{{__('Ticket Products')}}</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link" href="{{url('/admin/pages')}}" aria-expanded="false">
                        <i class="fa fa-file"></i>
                        <span class="hide-menu">{{__('Mange pages')}}</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link" href="{{url('/admin/integrations')}}" aria-expanded="false">
                        <i class="fa fa-share"></i>
                        <span class="hide-menu">{{__('Integrations')}}</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link" href="{{url('/admin/settings')}}" aria-expanded="false">
                        <i class="fa fa-cog"></i>
                        <span class="hide-menu">{{__('Settings')}}</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>

