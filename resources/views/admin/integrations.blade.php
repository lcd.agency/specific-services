@extends('admin.layouts.app')
@section('title', 'Manage integrations')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{__('Integrations')}}</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}" class="text-muted">{{__('Dashboard')}}</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">{{__('integrations')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    @if(session('success'))
    <div class="note note-success">
        <span class="icon"><i class="fa fa-check"></i></span>
        {{session('success')}}
    </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('admin.pages.integrations')}}" method="POST">
                        @csrf
                        <h3 class="bg-primary text-white p-2"><i class="fab fa-facebook-f"></i> {{__('Facebook')}}</h3>
                        <div class="form-group">
                            <label for="FACEBOOK_CLIENT_ID">{{__('FACEBOOK CLIENT ID :')}}</label>
                            <input name="FACEBOOK_CLIENT_ID" type="text" class="form-control" value="{{$integrates->FACEBOOK_CLIENT_ID}}">
                        </div>
                        <div class="form-group">
                            <label for="FACEBOOK_CLIENT_SECRET">{{__('FACEBOOK CLIENT SECRET :')}}</label>
                            <input name="FACEBOOK_CLIENT_SECRET" type="text" class="form-control" value="{{$integrates->FACEBOOK_CLIENT_SECRET}}">
                        </div>
                        <div class="form-group">
                            <label for="FACEBOOK_REDIRECT_URL">{{__('FACEBOOK REDIRECT URL :')}}</label>
                            <input name="FACEBOOK_REDIRECT_URL" type="text" class="form-control" value="{{$integrates->FACEBOOK_REDIRECT_URL}}">
                            <small class="text-muted">{{__('This is your URL :')}} {{url('/third-party/facebook/callback')}}</small>
                        </div>
                        <h3 class="bg-danger text-white p-2"><i class="fab fa-google"></i> {{__('Google')}}</h3>
                        <div class="form-group">
                            <label for="GOOGLE_CLIENT_ID">{{__('GOOGLE CLIENT ID :')}}</label>
                            <input name="GOOGLE_CLIENT_ID" type="text" class="form-control" value="{{$integrates->GOOGLE_CLIENT_ID}}">
                        </div>
                        <div class="form-group">
                            <label for="GOOGLE_CLIENT_SECRET">{{__('GOOGLE CLIENT SECRET :')}}</label>
                            <input name="GOOGLE_CLIENT_SECRET" type="text" class="form-control" value="{{$integrates->GOOGLE_CLIENT_SECRET}}">
                        </div>
                        <div class="form-group">
                            <label for="GOOGLE_REDIRECT">{{__('GOOGLE REDIRECT URL :')}}</label>
                            <input name="GOOGLE_REDIRECT" type="text" class="form-control" value="{{$integrates->GOOGLE_REDIRECT}}">
                            <small class="text-muted">{{__('This is your URL :')}} {{url('/third-party/google/callback')}}</small>
                        </div>
                        <button type="submit" class="btn btn-primary">{{__('Save changes')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop