<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('admin.includes.head')
</head>
<body>
    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
    @include('admin.includes.header')
    @include('admin.includes.sidebar')
    <div class="page-wrapper">
    @yield('content')
    </div>
    </div>
    @include('admin.includes.footer')
</body>
</html>