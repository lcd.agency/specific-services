@extends('admin.layouts.app')
@section('title', 'Manage pages')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{__('Pages')}}</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}" class="text-muted">{{__('Dashboard')}}</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">{{__('Pages')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    @if(session('success'))
    <div class="note note-success">
        <span class="icon"><i class="fa fa-check"></i></span>
        {{session('success')}}
    </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('admin.pages.update')}}" method="POST">
                        @csrf
                        <div class="form-group">
                        <label for="privacy" class="text-dark"><strong>Privacy policy :</strong></label>
                        <span class="float-right"><a target="_blank" href="{{url('/privacy')}}" class="btn btn-success btn-sm">View</a></span>
                            <textarea type="text" id="ckeditor1" class="form-control" rows="10" name="privacy">{{$pages->privacy}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="terms" class="text-dark"><strong>Terms of use :</strong></label>
                            <span class="float-right"><a target="_blank" href="{{url('/terms')}}" class="btn btn-success btn-sm">View</a></span>
                            <textarea type="text" id="ckeditor2" class="form-control" rows="10" name="terms">{{$pages->terms}}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop