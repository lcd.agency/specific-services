@extends('admin.layouts.app')
@section('title', 'Products')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{__('Ticket Products')}}</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}" class="text-muted">{{__('Dashboard')}}</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">{{__('Products')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    @error('product_name')
    <div class="note note-danger">
        <span class="icon"><i class="fa fa-exclamation-triangle"></i></span>
        <strong>{{ $message }}</strong>
    </div>
    @enderror
    @if(session('success'))
    <div class="note note-success">
        <span class="icon"><i class="fa fa-check"></i></span>
        {{session('success')}}
    </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{__('All Produts')}} <span class="float-right">
                        <a href="#" data-toggle="modal" data-target="#addProduct" class="btn btn-primary btn-sm">
                            <i class="fa fa-plus"></i> {{__('Add new Product')}}
                        </a></span></h4>
                        <div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="addProductLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="addProductLabel">{{__('Add New Product')}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{route('admin/products/store')}}" method="POST">
                                            @csrf
                                            <div class="form-group">
                                                <label for="product_name"><small>{{__('Product Name :')}}</small></label>
                                                <input type="text" name="product_name" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary btn-sm">{{__('Save')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        </h4>
                        <h6 class="card-subtitle">{{__('Here you can add or remove all products that users will open tickets in.')}}</h6>
                        <div class="table-responsive">
                            <table id="default_order" class="table table-striped table-bordered no-wrap">
                                <thead>
                                    <tr>
                                        <th>{{__('#ID')}}</th>
                                        <th>{{__('Product Name')}}</th>
                                        <th class="text-center">{{__('Date / Time')}}</th>
                                        <th class="text-center">{{__('Edit / Delete')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $product)
                                    <tr>
                                        <td>{{$product->id}}</td>
                                        <td>{{$product->product_name}}</td>
                                        <td class="text-center">{{$product->created_at}}</td>
                                        <td class="text-center">
                                            <a href="{{url('/admin/products/edit/'.$product->id)}}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                            <a href="{{url('/admin/products/delete/'.$product->id)}}" onclick="return confirm('Are you sure ?');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop