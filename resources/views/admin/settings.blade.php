@extends('admin.layouts.app')
@section('title', 'Settings')
@section('content')
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-7 align-self-center">
      <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{__('Settings')}}</h4>
      <div class="d-flex align-items-center">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb m-0 p-0">
            <li class="breadcrumb-item"><a href="{{url('dashboard')}}" class="text-muted">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item text-muted active" aria-current="page">{{__('settings')}}</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  @if(session('success'))
  <div class="note note-success">
    <span class="icon"><i class="fa fa-check"></i></span>
    {{session('success')}}
  </div>
  @elseif(session('info'))
  <div class="note note-info">
    <span class="icon"><i class="fa fa-question-circle"></i></span>
    {{session('info')}}
  </div>
  @endif
  <div class="card ">
    <div class="card-body">
      <form action="{{route('admin/settings/update')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <h4 class="card-title">{{__('Website Settings')}} <span class="float-right">
          <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> {{__('Save changes')}}</button>
        </span></h4>
        <h6 class="card-subtitle">{{__('Update your site settings')}}</h6>
        <div class="form-group mt-4">
          <label for="site_name">{{__('Site Name ')}}<span style="color:red;">*</span>:</label>
          <input type="text" name="site_name" id="site_name" class="form-control @error('site_name') is-invalid @enderror" value="{{$settings->site_name}}" required>
          @error('site_name')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="row">
          <div class="col-lg-6">
            <div class="form-image">
              <img src="{{asset('/images/'.$settings->site_logo)}}" width="200">
            </div>
            <div class="form-group">
              <label for="logo">Logo :</label>
              <input type="file" name="site_logo" id="site_logo" class="form-control @error('site_logo') is-invalid @enderror">
              @error('site_logo')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="form-group">
              <label for="description">{{__('Description (optional) :')}}</label>
              <textarea name="description" id="description" class="form-control @error('description') is-invalid @enderror" rows="6">{{$settings->description}}</textarea>
              @error('description')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-image">
              <img src="{{asset('/images/'.$settings->site_fav)}}" width="50">
            </div>
            <div class="form-group">
              <label for="favicon">{{__('Favicon :')}}</label>
              <input type="file" name="site_fav" id="site_fav" class="form-control @error('site_fav') is-invalid @enderror">
              @error('site_fav')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
            <div class="form-group">
              <label for="keywords">{{__('Keywords (optional) :')}}</label>
              <textarea name="keywords" id="keywords" class="form-control @error('keywords') is-invalid @enderror" rows="6">{{$settings->keywords}}</textarea>
              @error('keywords')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@stop