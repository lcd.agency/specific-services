@extends('admin.layouts.app')
@section('title', 'View Ticket No.#'.$data->id)
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{__('Ticket No.#')}}{{$data->id}}</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}" class="text-muted">{{__('Dashboard')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/tickets')}}" class="text-muted">{{__('Tickets')}}</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">{{__('Ticket No.#')}}{{$data->id}}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5">
            <span class="float-right">
                <form action="{{route('admin/ticket/update')}}" method="POST">
                    @csrf
                    @if($data->status == 1 or $data->status == 2)
                    <input type="hidden" name="ticket_id" value="{{$data->id}}">
                <button type="submit" class="btn btn-primary btn-sm">{{__('Close Ticket')}}</a>
                @elseif($data->status == 3)
                <input type="hidden" name="ticketId" value="{{$data->id}}">
            <button type="submit" class="btn btn-success btn-sm">{{__('Re-open Ticket')}}</a>
            @endif
        </form>
    </span>
</div>
</div>
</div>
<div class="container-fluid">
@if(session('success'))
<div class="note note-success">
<span class="icon"><i class="fa fa-check"></i></span>
{{session('success')}}
</div>
@endif
<div class="row">
<div class="col-12">
    <div class="card">
        <div class="card-body">
            <span class="text-muted ttv"><i class="fa fa-user"></i> {{__('Opend by :')}} {{$data->user->firstname.' '.$data->user->lastname}}</span>
            <span class="text-muted ttv"><i class="far fa-clock"></i> {{__('at :')}} {{\Carbon\Carbon::parse($data->created_at)->diffForHumans() }}</span>
            <span class="text-muted ttv"><i class="fa fa-archive"></i> {{__('Product :')}} {{$data->product}}</span>
            <span class="text-muted float-right"><i class="fas fa-fire"></i> {{__('Priority :')}}
                @if($data->priority == 1)
                <span class="badge badge-pill badge-secondary">{{ __('Low')}}</span>
                @elseif($data->priority == 2)
                <span class="badge badge-pill badge-warning">{{ __('high')}}</span>
                @elseif($data->priority == 3)
                <span class="badge badge-pill badge-danger">{{ __('Urgent')}}</span>
                @endif
            </span>
            <h3 class="text-dark mt-2">{{$data->subject}}</h3>
            <p>{{$data->description}}</p>
            @if(!$data->attachfile == null)
            <a data-toggle="modal" data-target=".bd-example-modal-lg" href="" target="_blank" class="btn btn-outline-primary">
            <i class="fa fa-download"></i> {{ __('File attachment')}}</a>
            <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="ticketModal" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('File attachment')}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="{{asset('uploads/tickets/'.$data->attachfile)}}" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
        @if($replies->count())
        <div class="replies">
            @foreach($replies as $reply)
            <div class="auth__reply">
                <span class="reply__posted"><i class="far fa-clock"></i> {{__('Replied at :')}} {{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}</span>
                <h4 class="text-dark mt-2"><i class="fa fa-user"></i> {{__('by :')}} {{$reply->user->firstname}} {{$reply->user->lastname}}</h4>
                <p class="text-dark">{{$reply->replay_body}}</p>
                @if(!$reply->replay_file == null)
                <a data-toggle="modal" data-target=".rpl{{ $reply->id }}" href="" target="_blank" class="btn btn-outline-primary">
                <i class="fa fa-download"></i> {{ __('File attachment')}}</a>
                <div class="modal fade bd-example-modal-lg rpl{{ $reply->id }}" tabindex="-1" role="dialog" aria-labelledby="ticketModal" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">{{ __('File attachment')}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{asset('uploads/replies/'.$reply->replay_file)}}" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            @endforeach
        </div>
        @else
        <div class="nodata text-center pt-3 pb-3">
            <span class="text-muted">{{ __('No replies yet')}}</span>
        </div>
        @endif
    </div>
    <div class="card">
        <div class="card-body">
            @if($data->status == 3)
            <div class="text-center pt-3 pb-3">
                <span class="text-muted">{{ __('This ticket is now closed you cannot add any reply')}}</span>
            </div>
            @else
            <form action="{{route('admin/ticket/store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="ticket_id" value="{{$data->id}}">
                <div class="form-group">
                    <label for="replay_body">{{__('Your Replay : ')}}<span style="color:red;">*</span></label>
                    <textarea name="replay_body" class="form-control @error('replay_body') is-invalid @enderror"" rows="6" required></textarea>
                    @error('replay_body')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="replay_body">{{__('Your Replay (optional) : ')}}</label>
                    <input type="file" name="replay_file" class="form-control">
                </div>
                <button class="btn btn-primary" type="submit">{{__('Submit')}}</button>
            </form>
            @endif
        </div>
    </div>
</div>
</div>
</div>
@stop