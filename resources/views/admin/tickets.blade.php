@extends('admin.layouts.app')
@section('title', 'Tickets')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{__('Tickets')}}</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}" class="text-muted">{{__('Dashboard')}}</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">{{__('Tickets')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    @if(session('success'))
    <div class="note note-success">
        <span class="icon"><i class="fa fa-check"></i></span>
        {{session('success')}}
    </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-white">
                    <ul class="nav nav-pills card-header-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" id="opened-tab" data-toggle="tab" href="#opened" role="tab" aria-controls="opened" aria-selected="true">{{__('Opened Tickets')}} ( {{$open_tickets->count()}} )</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ansewred-tab" data-toggle="tab" href="#ansewred" role="tab" aria-controls="ansewred" aria-selected="false">{{__('Answered Tickets')}} ( {{$answered_tickets->count()}} )</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="closed-tab" data-toggle="tab" href="#closed" role="tab" aria-controls="closed" aria-selected="false">{{__('Closed Tickets')}} ( {{$closed_tickets->count()}} )</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="opened" role="tabpanel" aria-labelledby="opened-tab">
                            <div class="table-responsive">
                                <table id="default_order" class="table table-striped table-bordered no-wrap">
                                    <thead>
                                        <tr>
                                            <th>{{__('#ID')}}</th>
                                            <th>{{__('Subject')}}</th>
                                            <th>{{__('Date / Time')}}</th>
                                            <th>{{__('Status')}}</th>
                                            <th class="text-center">{{__('View Ticket')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($open_tickets as $open_ticket)
                                        <tr>
                                            <td>{{$open_ticket->id}}</td>
                                            <td>{{$open_ticket->subject}}</td>
                                            <td>{{$open_ticket->created_at}}</td>
                                            <td class="text-center">
                                                @if($open_ticket->status == 1)
                                                <span class="badge badge-pill badge-primary">{{ __('Opened')}}</span>
                                                @elseif($open_ticket->status == 2)
                                                <span class="badge badge-pill badge-success">{{ __('Answered')}}</span>
                                                @elseif($open_ticket->status == 3)
                                                <span class="badge badge-pill badge-danger">{{ __('Closed')}}</span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{url('/admin/ticket/'.$open_ticket->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="ansewred" role="tabpanel" aria-labelledby="ansewred-tab">
                            <div class="table-responsive">
                                <table id="default_order2" class="table table-striped table-bordered no-wrap">
                                    <thead>
                                        <tr>
                                            <th>{{__('#ID')}}</th>
                                            <th>{{__('Subject')}}</th>
                                            <th>{{__('Date / Time')}}</th>
                                            <th>{{__('Status')}}</th>
                                            <th class="text-center">{{__('View Ticket')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($answered_tickets as $answered_ticket)
                                        <tr>
                                            <td>{{$answered_ticket->id}}</td>
                                            <td>{{$answered_ticket->subject}}</td>
                                            <td>{{$answered_ticket->created_at}}</td>
                                            <td class="text-center">
                                                @if($answered_ticket->status == 1)
                                                <span class="badge badge-pill badge-primary">{{ __('Opened')}}</span>
                                                @elseif($answered_ticket->status == 2)
                                                <span class="badge badge-pill badge-success">{{ __('Answered')}}</span>
                                                @elseif($answered_ticket->status == 3)
                                                <span class="badge badge-pill badge-danger">{{ __('Closed')}}</span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{url('/admin/ticket/'.$answered_ticket->id)}}" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="closed" role="tabpanel" aria-labelledby="closed-tab">
                            <div class="table-responsive">
                                <table id="default_order3" class="table table-striped table-bordered no-wrap">
                                    <thead>
                                        <tr>
                                            <th>{{__('#ID')}}</th>
                                            <th>{{__('Subject')}}</th>
                                            <th>{{__('Date / Time')}}</th>
                                            <th>{{__('Status')}}</th>
                                            <th class="text-center">{{__('View Ticket')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($closed_tickets as $closed_ticket)
                                        <tr>
                                            <td>{{$closed_ticket->id}}</td>
                                            <td>{{$closed_ticket->subject}}</td>
                                            <td>{{$closed_ticket->created_at}}</td>
                                            <td class="text-center">
                                                @if($closed_ticket->status == 1)
                                                <span class="badge badge-pill badge-primary">{{ __('Opened')}}</span>
                                                @elseif($closed_ticket->status == 2)
                                                <span class="badge badge-pill badge-success">{{ __('Answered')}}</span>
                                                @elseif($closed_ticket->status == 3)
                                                <span class="badge badge-pill badge-danger">{{ __('Closed')}}</span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{url('/admin/ticket/'.$closed_ticket->id)}}" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@stop