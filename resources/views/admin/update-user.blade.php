@extends('admin.layouts.app')
@section('title', 'Update User')
@section('content')
<div class="container-fluid">
  <a href="{{url('/admin/users')}}" class="btn btn-outline-secondary btn-sm">
  <i class="fa fa-chevron-left "></i> {{__('Back to Users page')}}</a>
  <hr/>
  <div class="row">
    <div class="col-6 m-auto">
      @if(session('success'))
      <div class="note note-success">
        <span class="icon"><i class="fa fa-check"></i></span>
        {{session('success')}}
      </div>
      @elseif(session('error'))
      <div class="note note-danger">
        <span class="icon"><i class="fa fa-exclamation-triangle"></i></span>
        {{session('error')}}
      </div>
      @endif
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">{{__('Update User permission')}} </h4>
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td><strong>{{__('Full Name : ')}}</strong> {{$user->firstname}} {{$user->lastname}}</td>
              </tr>
              <tr>
                <td><strong>{{__('Email:')}}</strong> {{$user->email}}</td>
              </tr>
              <tr>
                <td><strong>{{__('Joined at :')}}</strong> {{$user->created_at}}</td>
              </tr>
              <tr>
                <td><strong>{{__('Currant permission : ')}}</strong>
                  @if($user->permission == 0)
                  <badge class="badge badge-success">{{__('Admin')}}</badge>
                  @else
                  <badge class="badge badge-secondary">{{__('User')}}</badge>
                  @endif
                </td>
              </tr>
            </tbody>
          </table>
          <form action="{{route('admin/users/update/store')}}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{$user->id}}">
            <div class="form-group">
              <label for="product_name"><small>{{__('Select permission :')}}</small></label>
              <select class="form-control" name="permission">
                <option value="0">{{__('Admin')}}</option>
                <option value="1">{{__('User')}}</option>
              </select>
            </div>
            <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  @stop