@extends('admin.layouts.app')
@section('title', 'Users')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">{{__('Users')}}</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}" class="text-muted">{{__('Dashboard')}}</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">{{__('Users')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{__('All Users ')}}</h4></h4>
                    <h6 class="card-subtitle">{{__('Here you can check your website users.')}}</h6>
                    <div class="table-responsive">
                        <table id="default_order" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th>{{__('#ID')}}</th>
                                    <th>{{__('Full Name')}}</th>
                                    <th>{{__('Email')}}</th>
                                    <th>{{__('Permission')}}</th>
                                    <th class="text-center">{{__('Joined at')}}</th>
                                    <th>{{__('Update')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->firstname}} {{$user->lastname}}</td>
                                    <td>{{$user->email}}
                                        @if(!$user->email_verified_at == null)
                                        <badge class="badge badge-success badge-pill">{{__('Verified')}}</badge>
                                        @else
                                        <badge class="badge badge-secondary badge-pill">{{__('Not verified')}}</badge>
                                        @endif
                                    </td>
                                    <td class="text-center">  @if($user->permission == 0)
                                        <badge class="badge badge-success badge-pill">{{__('Admin')}}</badge>
                                        @else
                                        <badge class="badge badge-secondary badge-pill">{{__('User')}}</badge>
                                    @endif</td>
                                    <td class="text-center">{{$user->created_at}}</td>
                                    <td class="text-center">
                                        <a href="{{url('/admin/users/update/'.$user->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop