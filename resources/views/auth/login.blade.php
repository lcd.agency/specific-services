@extends('layouts.auth')
@section('title', 'Login')
@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title">{{ __('Login') }} <span class="float-right"><a href="{{url('/register')}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-user-plus"></i> {{__('Create new account') }}</a></span></h4>
        <form action="{{ route('login') }}" method="POST" class="my-login-validation">
            @csrf
            <div class="form-group">
                <label for="email">{{ __('E-Mail Address :') }} <span class="fsgred">*</span></label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password">{{ __('Password :') }} <span class="fsgred">*</span>
                    @if (Route::has('password.request'))
                    <a class="float-right" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                    @endif
                </label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        
                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group m-0">
                <button type="submit" name="logBtn" class="btn btn-primary btn-block"> {{ __('Login') }}</button>
            </div>
            <div class="d-flex align-items-center my-3">
                <hr class="flex-grow-1">
                <span class="mx-2 text-2 text-muted font-weight-300">Or Login with</span>
                <hr class="flex-grow-1">
              </div>
            <div class="row">
                <div class="col-lg-6 mb-2">
                  <a href="{{route('third-party.action', 'facebook')}}" class="btn btn-primary btn-block btn-sm"><i class="fa fa-facebook"></i> Facebook</a>
                </div>
                 <div class="col-lg-6">
                    <a href="{{route('third-party.action', 'google')}}" class="btn btn-danger btn-block btn-sm"><i class="fa fa-google"></i> Google</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection