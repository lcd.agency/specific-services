@extends('layouts.auth')
@section('title', 'Confirm Password')
@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title">{{ __('Confirm Password') }}</h4>
        {{ __('Please confirm your password before continuing.') }}
        <form method="POST" action="{{ route('password.confirm') }}">
            @csrf
            <div class="form-group">
                <label for="password">{{ __('Password :') }} <span class="fsgred">*</span></label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-block">
                {{ __('Confirm Password') }}
                </button>
                @if (Route::has('password.request'))
                <a class="btn btn-link pt-3" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
                @endif
            </div>
        </form>
    </div>
</div>
@endsection