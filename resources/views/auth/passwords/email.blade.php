@extends('layouts.auth')
@section('title', 'Reset Password')
@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title">{{ __('Reset Password') }} <span class="float-right"><a href="{{url('/')}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-sign-in"></i> {{__('Login') }}</a></span></h4>
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        <form action="{{ route('password.email') }}" method="POST" class="my-login-validation">
            @csrf
            <div class="form-group">
                <label for="email">{{ __('E-Mail Address :') }} <span class="fsgred">*</span></label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group m-0">
                <button type="submit" name="ResBtn" class="btn btn-primary btn-block"> {{ __('Send Password Reset Link') }}</button>
            </div>
        </form>
    </div>
</div>
@endsection