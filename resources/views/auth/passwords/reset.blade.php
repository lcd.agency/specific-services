@extends('layouts.auth')
@section('title', 'Reset Password')
@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title">{{ __('Reset Password ') }}<span class="float-right"><a href="{{url('/')}}'" class="btn btn-outline-primary btn-sm"><i class="fa fa-sign-in"></i> {{__('Login') }}</a></span></h4>
        <form method="POST" action="{{ route('password.update') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group">
                <label for="email">{{ __('E-Mail Address :') }} <span class="fsgred">*</span></label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password">{{ __('Password :') }} <span class="fsgred">*</span></label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <div class="form-group">
                    <label for="password-confirm"">{{ __('Confirm Password :') }} <span class="fsgred">*</span></label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>
            </div>
            <div class="form-group m-0">
                <button type="submit" name="ResBtn" class="btn btn-primary btn-block"> {{ __('Reset Password') }}</button>
            </div>
        </form>
    </div>
</div>
@endsection