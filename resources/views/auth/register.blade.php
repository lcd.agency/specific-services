@extends('layouts.auth')
@section('title', 'Register')
@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title">{{ __('Register') }} <span class="float-right"><a href="{{url('/')}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-sign-in"></i> {{__('I Already have account') }}</a></span></h4>
        <form action="{{ route('register') }}" method="POST" class="my-login-validation">
            @csrf
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="firstname">{{ __('First Name :') }} <span class="fsgred">*</span></label>
                        <input id="firstname" type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname') }}" required autocomplete="firstname" autofocus>
                        @error('firstname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="lastname">{{ __('Last Name :') }} <span class="fsgred">*</span></label>
                        <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>
                        @error('lastname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="email">{{ __('E-Mail Address :') }} <span class="fsgred">*</span></label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password">{{ __('Password :') }} <span class="fsgred">*</span></label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password">{{ __('Confirm Password :') }} <span class="fsgred">*</span></label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="password_confirmation">
            </div>
            <div class="form-group m-0">
                <button type="submit" name="RegBtn" class="btn btn-primary btn-block"> {{ __('Register') }}</button>
            </div>
            <div class="d-flex align-items-center my-3">
                <hr class="flex-grow-1">
                <span class="mx-2 text-2 text-muted font-weight-300">Or Signup with</span>
                <hr class="flex-grow-1">
              </div>
            <div class="row">
                <div class="col-lg-6 mb-2">
                    <a href="{{route('third-party.action', 'facebook')}}" class="btn btn-primary btn-block btn-sm"><i class="fa fa-facebook"></i> Facebook</a>
                </div>
                 <div class="col-lg-6">
                    <a href="{{route('third-party.action', 'google')}}" class="btn btn-danger btn-block btn-sm"><i class="fa fa-google"></i> Google</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection