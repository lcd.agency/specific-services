<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>404 Page not found</title>
    <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Almarai:wght@300;400;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,300;0,400;0,500;1,300;1,400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('static/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('static/customize/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('static/fontawesome/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('static/fontawesome/font-awesome-animation.min.css') }}">
</head>
<body>
    <div class="container">
    <div class="error-page">
        <div class="row">
            <div class="col-lg-6 m-auto">
                <img src="{{url('/images/404.png')}}" class="img-fluid">
                <h1>Page Not Found</h1>
                <p>The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</p>
                <a href="{{url('/')}}" class="btn btn-primary"><i class="fa fa-home"></i> Back to home page</a>
            </div>
        </div>
    </div>
    </div>
</body>
</html>