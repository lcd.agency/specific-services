<footer class="fowtickets__main__footer text-center">
    <div class="footer-auth">
        {{__('Copyright')}} &copy; <script>document.write(new Date().getFullYear())</script> &mdash; {{$info->site_name}}
    </div>
</footer>
<script src="{{ asset('static/bootstrap/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('static/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ asset('static/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('static/customize/js/app.js') }}"></script>