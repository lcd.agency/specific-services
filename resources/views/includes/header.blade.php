<header class="footickets__main__header">
  <nav class="navbar navbar-expand-lg navbar-light bg-white">
    <div class="container">
      <a class="navbar-brand" href="{{url('/')}}">
        <img src="{{ asset('images/logo.png') }}" alt="logo" width="200px">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="fa fa-bars"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav m-auto edit-nav">
          <li class="nav-item">
            <a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="Dashboard" href="{{url('/dashboard')}}">
            <img src="{{ asset('images/dashboard.svg') }}" width="40" height="40"></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="My tickets" href="{{url('/tickets')}}">
            <img src="{{ asset('images/tickets.svg') }}" width="40" height="40"></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="New ticket" href="{{url('/open-ticket')}}">
            <img src="{{ asset('images/new.svg') }}" width="40" height="40"></a>
          </li>
          <li class="nav-item notice">
            <a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="Notifications" href="{{url('/notifications')}}">
            <img src="{{ asset('images/bell.svg') }}" width="40" height="40"></a>
            @if($notice->count() > 0)
            <i class="faa-flash animated fa fa-circle"></i>
            @endif
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="Edit profile" href="{{url('/edit-profile')}}">
            <img src="{{ asset('images/user.svg') }}" width="40" height="40"></a>
          </li>
          @if(Auth::user()->permission == 0)
          <li class="nav-item">
            <a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="Admin panel" href="{{url('/admin')}}">
            <img src="{{ asset('images/admin.svg') }}" width="40" height="40"></a>
          </li>
          @endif
        </ul>
        <ul class="navbar-nav navbar-nav-edit">
          <li class="nav-item dropdown user-dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img src="{{asset('/cdn/user/'.Auth::user()->avatar)}}" width="40" height="40" class="rounded-circle">
              <span>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              @if(Auth::user()->permission == 0)
              <a class="dropdown-item" href="{{url('/admin')}}"><i class="fa fa-user"></i> {{ __('Admin panel')}}</a>
              @endif
              <a class="dropdown-item" href="{{url('/edit-profile')}}"><i class="fa fa-edit"></i> {{ __('Edit profile')}}</a>
              <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> {{ __('Logout') }}</a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
              </form>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header>