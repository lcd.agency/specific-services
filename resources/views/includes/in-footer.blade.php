<footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__logo">
                            <a href="./index.html"><img src="{{url('images/'.$info->site_logo)}}" alt="{{$info->site_name}}" width="220"></a>
                        </div>
                        <p>{{$info->description}}</p>
                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1 col-md-3 col-sm-6">
                    <div class="footer__widget">
                        <h5>{{__('Important links')}}</h5>
                        <div class="footer__widget">
                            <ul>
                                <li><a href="{{url('/privacy')}}">{{__('Privacy Policy')}}</a></li>
                                <li><a href="{{url('/terms')}}">{{__('Terms of use')}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <div class="footer__widget">
                        <h5>{{__('Quick links')}}</h5>
                        <div class="footer__widget footer__widget--social">
                            <ul>
                                <li><a href="{{url('/tickets')}}">{{__('My tickets')}}</a></li>
                                <li><a href="{{url('/open-ticket')}}">{{__('Open ticket')}}</a></li>
                                <li><a href="{{url('/notifications')}}">{{__('Notifications')}}</a></li>
                                <li><a href="{{url('/edit-profile')}}">{{__('Edit profile')}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1 col-md-6 col-sm-6">
                    <div class="footer__widget footer__widget--address">
                        <h5>{{__('Account information')}}</h5>
                        <p>{{__('Here is your account information.')}}</p>
                        <ul>
                            <li><i class="fa fa-user"></i> {{__('Full name :')}} {{Auth::user()->firstname.' '.Auth::user()->lastname}}</li>
                            <li><i class="fa fa-envelope"></i> {{__('Email :')}} {{Auth::user()->email}}</li>
                            <li><i class="fa fa-clock-o"></i> {{__('Joined at :')}} {{ \Carbon\Carbon::parse(Auth::user()->created_at)->diffForHumans() }}</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer__copyright text-center">
                <div class="footer__copyright__text">
                     <p>{{__('Copyright ©')}}<script>document.write(new Date().getFullYear());</script> {{__('All rights reserved |')}} {{$info->site_name}}</p>
                </div>
            </div>
        </div>
    </footer>
<script src="{{ asset('static/bootstrap/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('static/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ asset('static/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('static/customize/js/app.js') }}"></script>