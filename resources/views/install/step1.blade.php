@extends('layouts.install')
@section('title', 'Step 1')
@section('content')
@if(session('error'))
<div class="note note-danger">
    {{session('error')}}
</div>
@endif
<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-2">{{ __('Database information') }} </h4>
        <p class="text-muted">{{ __('Enter your database information.') }}</p>
        <form action="{{route('install/step1/set_database')}}" method="POST">
            @csrf
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-database"></i></span>
                </div>
                <input type="text" class="form-control @error('database_name') is-invalid @enderror" placeholder="Database Name" name="database_name" required>
                @error('database_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="database_user_name"><i class="fa fa-user" style="font-size: 20px;"></i></span>
                </div>
                <input type="text" class="form-control @error('database_user_name') is-invalid @enderror" placeholder="Database Username" name="database_user_name" required>
                @error('database_user_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="database_password"><i class="fa fa-key"></i></span>
                </div>
                <input type="text" class="form-control @error('database_password') is-invalid @enderror" placeholder="Database Password" name="database_password">
                @error('database_password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="database_host_name"><i class="fa fa-home" style="font-size: 18px;"></i></span>
                </div>
                <input type="text" class="form-control @error('database_host_name') is-invalid @enderror" placeholder="Database Host Name" name="database_host_name" value="localhost" required>
                @error('database_host_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group m-0">
                <button type="submit" name="logBtn" class="btn btn-primary btn-block"> {{ __('Submit') }}</button>
            </div>
        </form>
    </div>
</div>
@stop