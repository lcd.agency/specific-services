@extends('layouts.install')
@section('title', 'Step 3')
@section('content')
@if(session('error'))
<div class="note note-danger">
    {{session('error')}}
</div>
@endif
<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-2">{{ __('Website information') }} </h4>
        <p class="text-muted">{{ __('Enter your website information.') }}</p>
        <form action="{{route('install/step3/set_siteinfo')}}" method="POST">
            @csrf
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-info-circle"></i></span>
                </div>
                <input type="text" class="form-control @error('site_name') is-invalid @enderror" placeholder="Website Name" name="site_name" required>
                @error('site_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-link"></i></span>
                </div>
                <input type="url" class="form-control @error('site_url') is-invalid @enderror" placeholder="Website URL" name="site_url" value="{{url('/')}}" required>
                @error('site_url')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group m-0">
                <button type="submit" name="logBtn" class="btn btn-primary btn-block"> {{ __('Submit') }}</button>
            </div>
        </form>
    </div>
</div>
@stop