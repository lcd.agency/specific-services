@extends('layouts.install')
@section('title', 'Step 4')
@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-2">{{ __('Admin information') }} </h4>
        <p class="text-muted">{{ __('Enter your admin information.') }}</p>
        <form action="{{route('install/step4/set_admininfo')}}" method="POST">
            @csrf
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope"></i></span>
                </div>
                <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Admin email" name="email" required>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-key"></i></span>
                </div>
                <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Admin password" name="password" required>
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-key"></i></span>
                </div>
                <input type="password" class="form-control" placeholder="Confirm password" name="password_confirmation" required>

            </div>
            <div class="form-group m-0">
                <button type="submit" name="logBtn" class="btn btn-primary btn-block"> {{ __('Finish Installation') }}</button>
            </div>
        </form>
    </div>
</div>
@stop