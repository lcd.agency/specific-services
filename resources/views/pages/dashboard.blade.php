@extends('layouts.default')
@section('title', 'Dashboard')
@section('content')
<div class="page__title">
	<h1>{{ __('Welcome back '.Auth::user()->firstname).' !'}}</h1>
</div>
<div class="fowtickets__main__content">
	<div class="container">
		<div class="fowtickets__boxes">
			<div class="row">
				<div class="bbx col-lg-4">
					<div class="box">
						<h1>{{$open_tickets_count->count()}}</h1>
						<p>{{ __('Opened Tickets')}}</p>
					</div>
				</div>
				<div class="bbx col-lg-4">
					<div class="box">
						<h1>{{$answered_tickets_count->count()}}</h1>
						<p>{{ __('Answered Tickets')}}</p>
					</div>
				</div>
				<div class="bbx col-lg-4">
					<div class="box">
						<h1>{{$closed_tickets->count()}}</h1>
						<p>{{ __('Closed Tickets')}}</p>
					</div>
				</div>
			</div>
		</div>
		<div class="fowtickets__last__tickets">
			<div class="row">
				<div class="col-lg-6">
					<div class="card edit-card">
						<div class="card-header">
							<i class="fa fa-folder-open" aria-hidden="true" style="color:#ff641a;"></i>
							{{ __('Last Opened Tickets')}}
							<span class="float-right">
								<a href="{{url('/tickets#opened')}}" class="btn btn-outline-primary btn-sm">{{ __('View all')}}</a>
							</span>
						</div>
						<div class="card-body">
							@if($open_tickets->count())
							@foreach($open_tickets as $open_ticket)
							<div class="ticket" onclick="window.location.href='{{url('ticket')}}/{{$open_ticket->id}}'">
								<div class="row">
									<div class="col-lg-8">
										<h5>{{$open_ticket->subject}}</h5>
									</div>
									<div class="col-lg-4">
										@if($open_ticket->status == 1)
										<span class="float-right badge badge-pill badge-primary">{{ __('Opened')}}</span>
										@elseif($open_ticket->status == 2)
										<span class="float-right badge badge-pill badge-success">{{ __('Answered')}}</span>
										@elseif($open_ticket->status == 3)
										<span class="float-right badge badge-pill badge-danger">{{ __('Closed')}}</span>
										@endif
									</div>
								</div>
								<small class="text-muted ppx"><i class="fa fa-info-circle"></i> {{__('Ticket ID : '.$open_ticket->id)}}</small>
								<small class="text-muted ppx"><i class="fa fa-clock-o"></i> {{$open_ticket->created_at->diffForHumans()}}</small>
								<small class="text-muted"><i class="fa fa-archive"></i> {{ __('Product : ')}} {{$open_ticket->product}} </small>
							</div>
							@endforeach
							@else
							<div class="text-center pt-3 pb-3">
								<span class="text-muted">{{ __('No data found')}}</span>
							</div>
							@endif
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="card edit-card">
						<div class="card-header">
							<i class="fa fa-envelope-open" aria-hidden="true" style="color:#ff641a;"></i>
							{{__('Last Answered Tickets')}} <span class="float-right"><a href="{{url('/tickets#answered')}}" class="btn btn-outline-primary btn-sm">{{ __('View all')}}</a></span></div>
							<div class="card-body">
								@if($answered_tickets->count())
								@foreach($answered_tickets as $answered_ticket)
								<div class="ticket" onclick="window.location.href='{{url('ticket')}}/{{$answered_ticket->id}}'">
									<div class="row">
										<div class="col-lg-8">
											<h5>{{$answered_ticket->subject}}</h5>
										</div>
										<div class="col-lg-4">
											@if($answered_ticket->status == 1)
											<span class="float-right badge badge-pill badge-primary">{{ __('Opened')}}</span>
											@elseif($answered_ticket->status == 2)
											<span class="float-right badge badge-pill badge-success">{{ __('Answered')}}</span>
											@elseif($answered_ticket->status == 3)
											<span class="float-right badge badge-pill badge-danger">{{ __('Closed')}}</span>
											@endif
										</div>
									</div>
									<small class="text-muted ppx"><i class="fa fa-info-circle"></i> {{__('Ticket ID : '.$answered_ticket->id)}}</small>
									<small class="text-muted ppx"><i class="fa fa-clock-o"></i> {{$answered_ticket->created_at->diffForHumans()}}</small>
									<small class="text-muted"><i class="fa fa-archive"></i> {{ __('Product : ')}} {{$answered_ticket->product}} </small>
								</div>
								@endforeach
								@else
								<div class="text-center pt-3 pb-3">
									<span class="text-muted">{{ __('No data found')}}</span>
								</div>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop