@extends('layouts.default')
@section('title', 'Edit Profile')
@section('content')
<div class="page__title">
	<h1>{{ __('Edit profile information') }}</h1>
</div>
<div class="fowtickets__main__content">
	<div class="container">
		@if(session('success'))
		<div class="note note-success">
			<span class="icon"><i class="fa fa-check"></i></span>
			{{session('success')}}
		</div>
		@elseif (session('error'))
		<div class="note note-danger">
			{{ session('error') }}
		</div>
		@endif
		<div class="card mb-3">
			<div class="card-body">
				<form action="{{ route('edit-profile')}}" method="POST" enctype="multipart/form-data">
					@csrf
					<h4 class="card-title">{{__('My profile')}}<span class="float-right"><button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> {{__('Save changes') }}</button></span></h4>
					<div class="row">
						<div class="col-lg-4">
							<div class="user-image text-center">
								<img src="{{asset('/cdn/user/'.Auth::user()->avatar)}}" width="120" height="120">
								<div class="form-group">
							       <input type="file" name="avatar" class="border-0 form-control  @error('avatar') is-invalid @enderror">
								   @error('avatar')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
								<small class="text-muted">Allowed (JPG, PNG, JPEG)</small>
							</div>
						</div>
						<div class="col-lg-8">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label for="firstname">{{ __('First Name :')}} <span class="fsgred">*</span></label>
										<input type="firstname" name="firstname" class="form-control @error('firstname') is-invalid @enderror" value="{{ Auth::user()->firstname }}" required>
										@error('firstname')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
										@enderror
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label for="lastname">{{ __('Last Name:')}} <span class="fsgred">*</span></label>
										<input type="lastname" name="lastname" class="form-control  @error('lastname') is-invalid @enderror" value="{{ Auth::user()->lastname }}" required>
										@error('lastname')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
										@enderror
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="email">{{ __('Email Address :')}} <span class="fsgred">*</span></label>
								<input type="email" name="email" class="form-control  @error('email') is-invalid @enderror" value="{{ Auth::user()->email }}" required>
								@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="card mb-5">
			<div class="card-body">
				<form action="{{route('edit-profile/update/password')}}" method="POST">
					@csrf
					<h4 class="card-title">{{__('Change Password')}} <span class="float-right"><button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> {{__('Save changes') }}</button></span></h4>
					<div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
						<label for="new-password" class="control-label">Current Password : <span class="fsgred">*</span></label>
						<input id="current-password" type="password" class="form-control @error('current-password') is-invalid @enderror" name="current-password" required>
						@error('current-password')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>
					<div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
						<label for="new-password" class="control-label">New Password : <span class="fsgred">*</span></label>
						<input id="new-password" type="password" class="form-control  @error('new-password') is-invalid @enderror" name="new-password" required>
						@error('new-password')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>
					<div class="form-group">
						<label for="new-password-confirm" class="control-label">Confirm New Password : <span class="fsgred">*</span></label>
						<input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" required>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop