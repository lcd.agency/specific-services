@extends('layouts.default')
@section('title', 'Notifications')
@section('content')
<div class="page__title">
  <h1>{{ __('Notifications ('.$notice->count()).')' }}</h1>
</div>
<div class="fowtickets__main__content mb-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 m-auto">
        @if($notice->count() > 0)
        @foreach($notice as $note)
        <div class="user_notice" onclick="window.location.href='{{url('ticket')}}/{{$note->id}}'">
          <div class="row">
            <div class="col-lg-2 text-center  d-none d-lg-block">
              <img src="{{ asset('images/bell.svg') }}" alt="Notifications">
            </div>
            <div class="col-lg-10">
              <span class="notice-text text-muted">{{__('New reply on your tiket')}}</span>
              <h5>{{$note->subject}}</h5>
            </div>
          </div>
        </div>
        @endforeach
        @else 
        <div class="text-center pt-3 pb-3 bg-white">
            <span class="text-muted">{{ __('You dont have Notifications')}}</span>
        </div>
        @endif
      </div>
    </div>
  </div>
</div>
@stop