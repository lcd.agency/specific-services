@extends('layouts.auth')
@section('title', 'Privacy policy')
@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title m-0">{{__('Privacy policy')}} 
        <span class="float-right"><a href="{{url('/')}}" class="btn btn-outline-primary btn-sm">
            <i class="fa fa-home"></i> {{__('Back to home')}}</a></span></h4>
        <hr/>
        {!! $privacy->privacy !!}
        
    </div>
</div>
@endsection