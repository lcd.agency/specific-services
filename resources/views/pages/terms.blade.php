@extends('layouts.auth')
@section('title', 'Terms of use')
@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title m-0">{{__('Terms of use')}}
        <span class="float-right"><a href="{{url('/')}}" class="btn btn-outline-primary btn-sm">
            <i class="fa fa-home"></i> {{__('Back to home')}}</a></span></h4>
        <hr/>
       {!! $terms->terms !!}
    </div>
</div>
@endsection