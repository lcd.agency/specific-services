<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Install routes
Route::get('/install/install', [App\Http\Controllers\Install\InstallController::class, 'index'])->name('install/install');
Route::get('/install/step1', [App\Http\Controllers\Install\InstallController::class, 'step1'])->name('install/step1');
Route::post('/install/step1/set_database', [App\Http\Controllers\Install\InstallController::class, 'set_database'])->name('install/step1/set_database');
Route::get('/install/step2', [App\Http\Controllers\Install\InstallController::class, 'step2'])->name('install/step2');
Route::post('/install/step2/import_database', [App\Http\Controllers\Install\InstallController::class, 'import_database'])->name('install/step2/import_database');
Route::get('/install/step3', [App\Http\Controllers\Install\InstallController::class, 'step3'])->name('install/step3');
Route::post('/install/step3/set_siteinfo', [App\Http\Controllers\Install\InstallController::class, 'set_siteinfo'])->name('install/step3/set_siteinfo');
Route::get('/install/step4', [App\Http\Controllers\Install\InstallController::class, 'step4'])->name('install/step4');
Route::post('/install/step4/set_admininfo', [App\Http\Controllers\Install\InstallController::class, 'set_admininfo'])->name('install/step4/set_admininfo');

// First check if website is installed
Route::group(['middleware' => 'check.installation'], function () {    
    // Authentication Routes Login & Register & Reset & Verify email
    Route::get('/', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm']);
    Route::get('/login', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm']);
    Route::post('/login', [App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login');
    Route::post('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
    Route::get('register', [App\Http\Controllers\Auth\RegisterController::class, 'showRegistrationForm'])->name('register');
    Route::post('register', [App\Http\Controllers\Auth\RegisterController::class, 'register']);
    Route::get('password/reset', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
    Route::post('password/email', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
    Route::get('password/reset/{token}', [App\Http\Controllers\Auth\ResetPasswordController::class, 'showResetForm'])->name('password.reset');
    Route::post('password/reset', [App\Http\Controllers\Auth\ResetPasswordController::class, 'reset'])->name('password.update');
    Route::get('password/confirm', [App\Http\Controllers\Auth\ConfirmPasswordController::class, 'showConfirmForm'])->name('password.confirm');
    Route::post('password/confirm', [App\Http\Controllers\Auth\ConfirmPasswordController::class, 'confirm']);
    Route::get('email/verify', [App\Http\Controllers\Auth\VerificationController::class, 'show'])->name('verification.notice');
    Route::get('email/verify/{id}/{hash}', [App\Http\Controllers\Auth\VerificationController::class, 'verify'])->name('verification.verify');
    Route::post('email/resend', [App\Http\Controllers\Auth\VerificationController::class, 'resend'])->name('verification.resend');

    // Privacy and terms pages 
    Route::get('/privacy', [App\Http\Controllers\Pages\PagesController::class, 'privacy']);
    Route::get('/terms', [App\Http\Controllers\Pages\PagesController::class, 'terms']);
    
    //** / Enable email verfication
    //*** Auth::routes(['verify' => true]);

    Route::group(['middleware' => 'CheckForDetailsExists'], function () {   
        // Social logins routes
        Route::get('third-party/{provider}', [App\Http\Controllers\Auth\SocialController::class, 'redirectToProvider'])->name('third-party.action');
        Route::get('third-party/{provider}/callback', [App\Http\Controllers\Auth\SocialController::class, 'handleProviderCallback']);
        // Social login setpassword 
        Route::get('/addition/password', [App\Http\Controllers\Addition\AddPasswordController::class, 'index'])->name('addition.password');
        Route::post('/addition/password', [App\Http\Controllers\Addition\AddPasswordController::class, 'update'])->name('addition.password');
        // Social login setemail if there is no email
        Route::get('/addition/email', [App\Http\Controllers\Addition\AddEmailController::class, 'index'])->name('addition.email');
        Route::post('/addition/email', [App\Http\Controllers\Addition\AddEmailController::class, 'update'])->name('addition.email');
    
    });
    // Middleware for check if there is auth email and password 
    Route::group(['middleware' => 'CheckFordetails'], function () {
        // User dashboard route
        Route::get('dashboard', [App\Http\Controllers\Pages\DashboardController::class, 'index'])->name('dashboard');

        // User edit profile 
        Route::get('edit-profile', [App\Http\Controllers\Pages\ProfileController::class, 'index'])->name('edit-profile');
        Route::post('edit-profile', [App\Http\Controllers\Pages\ProfileController::class, 'update'])->name('edit-profile');
        // User update password
        Route::post('edit-profile/update/password', [App\Http\Controllers\Pages\ProfileController::class, 'UpdatePassword'])->name('edit-profile/update/password');

        // User open Tciket routes    
        Route::get('open-ticket', [App\Http\Controllers\Pages\OpenticketController::class, 'index'])->name('open-ticket');
        Route::post('open-ticket', [App\Http\Controllers\Pages\OpenticketController::class, 'InsertTicket'])->name('open-ticket');

        // User show all tickets route
        Route::get('tickets', [App\Http\Controllers\Pages\TicketsController::class, 'TicketsData'])->name('tickets');
        Route::get('tickets/search', [App\Http\Controllers\Pages\TicketsController::class, 'search'])->name('search.tickets');

        // User view ticket & update status & add replies
        Route::get('ticket', [App\Http\Controllers\Pages\TicketsController::class, 'backTotickets'])->name('ticket');
        Route::get('ticket/{id}', [App\Http\Controllers\Pages\ViewticketController::class, 'ViewTicketData'])->name('ticket');
        Route::post('ticket/update', [App\Http\Controllers\Pages\ViewticketController::class, 'UpdateTicketData'])->name('ticket/update');
        Route::post('ticket/store', [App\Http\Controllers\Pages\RepliesController::class, 'addRepleyData'])->name('ticket/store');

        // User notifications route
        Route::get('notifications', [App\Http\Controllers\Pages\NotificationsController::class, 'index'])->name('notifications');

        // Admin routes 
        Route::group(['middleware' => 'admin'], function () {
            // Admin dashboard
            Route::get('admin/dashboard', [App\Http\Controllers\Admin\AdminDashboardController::class, 'index']);
            Route::get('admin', [App\Http\Controllers\Admin\AdminDashboardController::class, 'RedirectToDashboard']);
            // Admin Mange products adit & update & delete
            Route::get('admin/products', [App\Http\Controllers\Admin\ProductsController::class, 'index']);
            Route::post('admin/products/store', [App\Http\Controllers\Admin\ProductsController::class, 'AddProducts'])->name('admin/products/store');
            Route::get('admin/products/delete/{id}', [App\Http\Controllers\Admin\ProductsController::class, 'DeleteProduct']);
            Route::get('admin/products/edit/{id}', [App\Http\Controllers\Admin\ProductsController::class, 'EditProduct']);
            Route::post('admin/products/edit', [App\Http\Controllers\Admin\ProductsController::class, 'UpdateProduct'])->name('admin/products/edit');
            // Admin webiste Settings
            Route::get('admin/settings', [App\Http\Controllers\Admin\SettingsController::class, 'index']);
            Route::post('admin/settings/update', [App\Http\Controllers\Admin\SettingsController::class, 'UpdateData'])->name('admin/settings/update');
            // Admin manage tickets View & add reply & close ticket
            Route::get('admin/tickets', [App\Http\Controllers\Admin\TicketsController::class, 'index']);
            Route::get('admin/tickets/delete/{id}', [App\Http\Controllers\Admin\TicketsController::class, 'DeleteTicket']);
            Route::get('admin/ticket', [App\Http\Controllers\Admin\TicketController::class, 'BackToTickets']);
            Route::get('admin/ticket/{id}', [App\Http\Controllers\Admin\TicketController::class, 'index']);
            Route::post('admin/ticket/update', [App\Http\Controllers\Admin\TicketController::class, 'update'])->name('admin/ticket/update');
            Route::post('admin/ticket/store', [App\Http\Controllers\Admin\RepliesController::class, 'store'])->name('admin/ticket/store');
            // Admin manage users & update permission
            Route::get('admin/users', [App\Http\Controllers\Admin\UsersController::class, 'index']);
            Route::get('admin/users/update/{id}', [App\Http\Controllers\Admin\UsersController::class, 'UserData']);
            Route::post('admin/users/update/store', [App\Http\Controllers\Admin\UsersController::class, 'update'])->name('admin/users/update/store');
            // Admin manage pages 
            Route::get('admin/pages', [App\Http\Controllers\Admin\PagesController::class, 'index']);
            Route::post('admin/pages', [App\Http\Controllers\Admin\PagesController::class, 'update'])->name('admin.pages.update');
            // Admin manage Integrations facebook & Google
            Route::get('admin/integrations', [App\Http\Controllers\Admin\IntegrationsController::class, 'index']);
            Route::post('admin/integrations', [App\Http\Controllers\Admin\IntegrationsController::class, 'update'])->name('admin.pages.integrations');
        });
    });
});